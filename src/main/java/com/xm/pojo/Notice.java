package com.xm.pojo;
public class Notice {
    private String name;
    private String information;
    private String noticeTime;
    private int flag;
    private String account;

    private Integer noticeId;

    public Notice(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public String getNoticeTime() {
        return noticeTime;
    }

    public void setNoticeTime(String noticeTime) {
        this.noticeTime = noticeTime;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Integer getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(int noticeId) {
        this.noticeId = noticeId;
    }

    @Override
    public String toString() {
        return "Notice{" +
                "name='" + name + '\'' +
                ", information='" + information + '\'' +
                ", noticeTime='" + noticeTime + '\'' +
                ", flag=" + flag +
                ", account='" + account + '\'' +
                ", noticeId=" + noticeId +
                '}';
    }
}