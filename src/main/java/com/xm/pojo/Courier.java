package com.xm.pojo;
public class Courier {
    private int courierId;
    private String courierName;
    private String sex;
    private int number;
    private String telephone;
    private double money;
    private String account;
    private String password;

    public Courier(){

    }

    public int getCourierId() {
        return courierId;
    }

    public void setCourierId(int courierId) {
        this.courierId = courierId;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Courier{" +
                "courierId=" + courierId +
                ", courierName='" + courierName + '\'' +
                ", sex='" + sex + '\'' +
                ", number=" + number +
                ", telephone='" + telephone + '\'' +
                ", money=" + money +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
