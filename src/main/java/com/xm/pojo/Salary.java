package com.xm.pojo;

public class Salary {
    private String date;
    private int numbers;
    private int moneys;
    private String courierName;

    public Salary(String date, int numbers, int moneys, String courierName) {
        this.date = date;
        this.numbers = numbers;
        this.moneys = moneys;
        this.courierName = courierName;
    }

    public Salary() {
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getNumbers() {
        return numbers;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }

    public int getMoneys() {
        return moneys;
    }

    public void setMoneys(int moneys) {
        this.moneys = moneys;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    @Override
    public String toString() {
        return "Salary{" +
                "date='" + date + '\'' +
                ", numbers=" + numbers +
                ", moneys=" + moneys +
                ", courierName='" + courierName + '\'' +
                '}';
    }
}
