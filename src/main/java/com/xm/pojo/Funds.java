package com.xm.pojo;

public class Funds {
    private String merchantName;
    private String courierName;
    private String date;
    private double expend;
    private double income;
    private double money;

    public Funds() {

    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getCourierName() {
        return courierName;
    }

    public void setCourierName(String courierName) {
        this.courierName = courierName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getExpend() {
        return expend;
    }

    public void setExpend(double expend) {
        this.expend = expend;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(double income) {
        this.income = income;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    @Override
    public String toString() {
        return "Funds{" +
                "merchantName='" + merchantName + '\'' +
                ", courierName='" + courierName + '\'' +
                ", date='" + date + '\'' +
                ", expend=" + expend +
                ", income=" + income +
                ", money=" + money +
                '}';
    }
}
