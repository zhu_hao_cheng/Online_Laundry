package com.xm.pojo;

public class Clothes {
    private int clothesId;
    private String clothesName;
    private double clothesMoney;

    public Clothes() {

    }

    public int getClothesId() {
        return clothesId;
    }

    public void setClothesId(int clothesId) {
        this.clothesId = clothesId;
    }

    public String getClothesName() {
        return clothesName;
    }

    public void setClothesName(String clothesName) {
        this.clothesName = clothesName;
    }

    public double getClothesMoney() {
        return clothesMoney;
    }

    public void setClothesMoney(double clothesMoney) {
        this.clothesMoney = clothesMoney;
    }

    @Override
    public String toString() {
        return "Clothes{" +
                "clothesId=" + clothesId +
                ", clothesName='" + clothesName + '\'' +
                ", clothesMoney=" + clothesMoney +
                '}';
    }
}
