package com.xm.pojo;
public class Merchant {
    private int merchantId;
    private String merchantName;
    private int good;
    private int differ;
    private String telephone;
    private double money;
    private String account;
    private String password;
    private int numbers;

    public Merchant(){

    }

    public int getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(int merchantId) {
        this.merchantId = merchantId;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public int getGood() {
        return good;
    }

    public void setGood(int good) {
        this.good = good;
    }

    public int getDiffer() {
        return differ;
    }

    public void setDiffer(int differ) {
        this.differ = differ;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNumbers() {
        return numbers;
    }

    public void setNumbers(int numbers) {
        this.numbers = numbers;
    }

    @Override
    public String toString() {
        return "Merchant{" +
                "merchantId=" + merchantId +
                ", merchantName='" + merchantName + '\'' +
                ", good=" + good +
                ", differ=" + differ +
                ", telephone='" + telephone + '\'' +
                ", money=" + money +
                ", account='" + account + '\'' +
                ", password='" + password + '\'' +
                ", numbers=" + numbers +
                '}';
    }
}
