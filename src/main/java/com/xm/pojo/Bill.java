package com.xm.pojo;
public class Bill {
    private int billId;
    private String account;
    private double money;
    private String billTime;

    public Bill(){

    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public String getBillTime() {
        return billTime;
    }

    public void setBillTime(String billTime) {
        this.billTime = billTime;
    }

    @Override
    public String toString() {
        return "Bill{" +
                "billId=" + billId +
                ", account='" + account + '\'' +
                ", money=" + money +
                ", billTime='" + billTime + '\'' +
                '}';
    }
}
