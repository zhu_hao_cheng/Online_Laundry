package com.xm.mapper;

import com.xm.pojo.Merchant;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface MerchantMapper {
    public Merchant queryByAccount(String account);
    public Merchant queryByMerchantId(int merchantId);
    public Integer changeMoney(Map map);
    public Integer feedBack(Map map);

    public boolean updateN(@Param("numbers") int numbers,@Param("merchant_id") int merchantId);
    public boolean updateM(@Param("money") double money,@Param("merchant_id") int merchantId);

    //修改密码
    public Integer modifyPasswordByAccount(Merchant merchant);
    //插入店家流水
    public Integer addFount(Map<Object,Object> map);
}
