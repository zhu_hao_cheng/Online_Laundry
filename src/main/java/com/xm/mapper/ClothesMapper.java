package com.xm.mapper;

import com.xm.pojo.Clothes;

import java.util.List;

public interface ClothesMapper {
    public List<Clothes> showClothesAll();

    public Clothes showClothesByClothesName(String clothesName);

    public int addClothes(Clothes clothes);

    public int updateClothesDataById(Clothes clothes);

    public int deleteClothesById(Integer clothesId);
}
