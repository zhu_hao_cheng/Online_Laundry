package com.xm.mapper;

import com.xm.pojo.Notice;

import java.util.List;
import java.util.Map;

public interface NoticeMapper {

    List<Notice> queryNoticeByUser(String account);

    List<Notice> queryNoticeByCourier(String account);

    List<Notice> queryNoticeByMerchant(String account);

    void addNotice(Map noticeMap);

    int updateFlag(Integer noticeId);
}
