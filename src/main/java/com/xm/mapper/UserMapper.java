package com.xm.mapper;

import com.xm.pojo.Courier;
import com.xm.pojo.Merchant;
import com.xm.pojo.User;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

public interface UserMapper {
    public User queryByAccount(String account);
    //注册用户
    public int registerAccount(User user);
    //更新用户信息
    public int updateUser(User user);
    //修改密码
    public Integer modifyPasswordByAccount(User user);
    public void updateMoney(Map<String,Object> map);
}
