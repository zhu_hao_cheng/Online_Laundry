package com.xm.mapper;

import com.xm.pojo.Courier;
import com.xm.pojo.Merchant;
import com.xm.pojo.Order;
import com.xm.pojo.Salary;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

public interface CourierMapper {
    public Courier queryByAccount(String account);
    public Courier queryByCourierId(int courierId);
    public Integer saveData(Map data);
    public Integer wipeData(String courierAccount);
    //查找配送员的流水
    public List<Salary> queryCourierMoney(int courierId);
    public List<Courier> queryAllCourier();
    public List<Order> queryOrdersByCourierId(int courierId);
    public boolean updateM(@Param("money") double money, @Param("courier_id") int courierId);

    public int deleteCourier(int courierId);
    public int updateCourier(Map<Object,Object> updateInformation);

    //修改密码
    public Integer modifyPasswordByAccount(Courier courier);
    public Integer addCourier(Courier courier);
}
