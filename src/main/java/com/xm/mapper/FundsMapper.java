package com.xm.mapper;

import com.xm.pojo.Funds;

import java.util.List;

public interface FundsMapper {
    List<Funds> showFundsAll();

    String showIncome(String date);

    String showIncomeAll();
}
