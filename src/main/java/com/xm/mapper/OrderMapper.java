package com.xm.mapper;

import com.xm.pojo.Order;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrderMapper {
    public List<Order> queryOrder(String account);
    public boolean addOrder(@Param("merchant_id") int merchantId, @Param("account") String account, @Param("state_id") int stateId, @Param("begin_time") String beginTime, @Param("money") double money);
    public boolean updateSC(@Param("state_id")int stateId,@Param("courier_id")int courierId,@Param("order_id")int orderId);
    public boolean updateS(@Param("state_id")int stateId,@Param("order_id")int orderId);
    public boolean updateET(@Param("end_time")String endTime,@Param("order_id")int orderId);
    public double getMoneyById(int orderId);
    public List<Order> queryGrabOrdersList();

    public List<Order> queryAllOrder();
    public List<Order> queryOrdersByCourierId(int courierId);

    //通过订单id查询订单
    Order queryOrdersByOrderId(int orderId);

    //订单完成后增加员工接单数
    int updateOrdersNumber(int number,int courierId);
}
