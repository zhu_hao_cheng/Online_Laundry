package com.xm.service;
import com.xm.pojo.Courier;
import com.xm.pojo.Order;
import com.xm.pojo.Salary;
import java.util.List;
import java.util.Map;


public interface CourierService {
    public List<Salary> queryCourierMoney(int courierId);
    public List<Courier>  queryAllCourier();
    public Map<Object,Object> deleteCourier(int courierId);
    public Courier findCourier(int courierId);
    public Map<Object,Object> updateCourier(int courierId,String courierName,String sex,String telephone);
    public Map<Object,Object> addCourier(String courierName,String sex,String telephone,String account,String password);

}
