package com.xm.service;

import java.util.Map;

public interface ClothesService {
    //展示衣物表
    public Map showClothesAll();

    //添加衣物
    public Map addClothes(String clothesName,double clothesMoney);

    //更新衣物信息
    public Map updateClothesDataById(Integer clothesId,String clothesName,double clothesMoney);

    //删除衣物信息
    public Map deleteClothesById(Integer clothesId);
}
