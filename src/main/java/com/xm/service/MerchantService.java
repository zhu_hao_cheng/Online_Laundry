package com.xm.service;

import com.xm.pojo.Merchant;

import java.util.List;
import java.util.Map;

public interface MerchantService {
    public Map<Object,Object> totalChangeMoney(int courierId,int merchantId);
    public Integer feedBack(int orderId,int flag,int merchantId);
    public Merchant querymerchantBymerchantId(int merchantId);
}
