package com.xm.service;
import com.xm.pojo.Order;
import java.util.List;
import java.util.Map;

public interface OrderService {
    public List<Order> queryOrder(String account);
    public List<Order> grabOrdersList();
    public Map addOrder(int merchantId, String account, double money);
    public Map updateSC(int orderId,int courierId);
    public Map updateS1(int orderId);
    public Map updateS2(int orderId);
    public Map updateS3(int orderId);
    public Map updateS4(int orderId);

    //带送回订单
    public Map updateS5(int orderId);

    public List<Order> queryAllOrder();
    public List<Order> queryOrdersByCourierId(int courierId);


}
