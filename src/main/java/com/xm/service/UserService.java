package com.xm.service;

import com.xm.pojo.User;

import java.util.Map;

public interface UserService {
    public Map queryByAccount(String account,String password);

    public Map register(String account, String user_name,String password);

    public Map updateUser(String userName, String address, String telephone, String account);

    //修改密码
    public Map modifyPasswordByAccount(String account,String oldPassword,String newPassword);

    //查询用户的所有信息
    public User queryByAccount(String account);
}
