package com.xm.service;

import java.util.Map;

public interface FundsService {
    //展示资金流水
    Map showFundsAll();

    //展示当月总收入
    Map showIncome(String dateTime);

    //展示从开店到现在总收入
    Map showIncomeAll();
}
