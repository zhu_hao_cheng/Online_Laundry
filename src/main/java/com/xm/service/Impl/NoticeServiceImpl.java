package com.xm.service.Impl;

import com.xm.mapper.CourierMapper;
import com.xm.mapper.MerchantMapper;
import com.xm.mapper.NoticeMapper;
import com.xm.mapper.UserMapper;
import com.xm.pojo.Courier;
import com.xm.pojo.Merchant;
import com.xm.pojo.Notice;
import com.xm.pojo.User;
import com.xm.service.NoticeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NoticeServiceImpl implements NoticeService {

    @Autowired
    NoticeMapper noticeMapper;

    @Autowired
    UserMapper userMapper;
    @Autowired
    CourierMapper courierMapper;
    @Autowired
    MerchantMapper merchantMapper;

    @Override
    public List<Notice> queryNoticeByAccount(String account) {
        char judge = account.charAt(0);
        try {
            if (judge == 'd') {
               List<Notice> list = noticeMapper.queryNoticeByMerchant(account);
                Merchant merchant = merchantMapper.queryByAccount(account);
                for(int i=0;i<list.size();i++){
                    list.get(i).setName(merchant.getMerchantName());
                    list.get(i).setAccount(merchant.getAccount());
                }
                return list;
            } else if (judge == 'p') {
                List<Notice> list = noticeMapper.queryNoticeByCourier(account);
                Courier courier = courierMapper.queryByAccount(account);
                for(int i=0;i<list.size();i++){
                    list.get(i).setName(courier.getCourierName());
                    list.get(i).setAccount(courier.getAccount());
                }
                return list;
            } else {
                List<Notice> list = noticeMapper.queryNoticeByUser(account);
                User user = userMapper.queryByAccount(account);
                for(int i=0;i<list.size();i++){
                    list.get(i).setName(user.getUserName());
                    list.get(i).setAccount(user.getAccount());
                }
                return list;
            }
        } catch (Exception e) {
            System.out.println(e.toString());
            return null;
        }

    }

    @Override
    public Map changeStage(Integer noticeId) {
        Map resultMap=new HashMap();
        try {
            int i =noticeMapper.updateFlag(noticeId);
            if(i!=0){
                resultMap.put("code",200);
                resultMap.put("msg","消息已读");
                return resultMap;
            }
            else{
                resultMap.put("code",500);
                resultMap.put("msg","读取失败");
                return resultMap;
            }
        }catch (Exception e){
            resultMap.put("code",501);
            resultMap.put("msg","系统异常");
            return resultMap;
        }

    }
}
