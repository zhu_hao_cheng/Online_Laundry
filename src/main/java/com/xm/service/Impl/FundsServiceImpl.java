package com.xm.service.Impl;

import com.xm.mapper.FundsMapper;
import com.xm.pojo.Funds;
import com.xm.service.FundsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FundsServiceImpl implements FundsService {
    @Autowired
    FundsMapper fundsMapper;

    @Override
    public Map showFundsAll() {
        Map resultMap = new HashMap();
        try {
            List<Funds> funds = fundsMapper.showFundsAll();
            if (funds != null){
                resultMap.put("code",200);
                resultMap.put("data", funds);
            }else {
                resultMap.put("code",501);
                resultMap.put("msg", "资金表获取失败或暂无数据");
            }
        }catch (Exception e) {
            e.printStackTrace();
            resultMap.put("code",500);
            resultMap.put("msg", "系统崩了，一会再来");
        }
        return resultMap;
    }

    @Override
    public Map showIncome(String dateTime) {
        Map resultMap = new HashMap();
        try {
            //2022-10-19
            //先判断是想查询年份还是月份
            if (dateTime.length()>5){
                //将日期提取到月份
                String date = dateTime.substring(0,7)+"%";
                String data = fundsMapper.showIncome(date);
                if (data != null) {
                    resultMap.put("code",200);
                    resultMap.put("data", data);
                }else {
                    resultMap.put("code",501);
                    resultMap.put("data", 0);
                }
            }else {
                //年收入
                String date = dateTime.substring(0,4)+"%";
                String data = fundsMapper.showIncome(date);
                if (data != null) {
                    resultMap.put("code",200);
                    resultMap.put("data", data);
                }else {
                    resultMap.put("code",200);
                    resultMap.put("data", 0);
                }
            }

        }catch (Exception e) {
            e.printStackTrace();
            resultMap.put("code",500);
            resultMap.put("msg", "系统崩了，一会再来");
        }
        return resultMap;
    }

    @Override
    public Map showIncomeAll() {
        Map resultMap = new HashMap();
        try {
            String data = fundsMapper.showIncomeAll();
            if (data != null){
                resultMap.put("code",200);
                resultMap.put("data", data);
            }else {
                resultMap.put("code",200);
                resultMap.put("data", 0);
            }
        }catch (Exception e) {
            e.printStackTrace();
            resultMap.put("code",500);
            resultMap.put("msg", "系统崩了，一会再来");
        }
        return resultMap;
    }
}
