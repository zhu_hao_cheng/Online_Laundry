package com.xm.service.Impl;

import com.xm.mapper.CourierMapper;
import com.xm.mapper.MerchantMapper;
import com.xm.mapper.UserMapper;
import com.xm.pojo.Courier;
import com.xm.pojo.Merchant;
import com.xm.pojo.User;
import com.xm.service.UserService;
import com.xm.utils.MD5Util;
import net.sf.json.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
@Service
public class UserServiceImpl implements UserService {

 @Autowired
    UserMapper userMapper;
 @Autowired
    MerchantMapper merchantMapper;

 @Autowired
    CourierMapper courierMapper;




    @Override
    public Map queryByAccount(String account,String password) {
        char flag= account.charAt(0);
        Map resultMap=new HashMap();
        try {
            if(flag=='d'){
                Merchant merchant = merchantMapper.queryByAccount(account);
                if (merchant == null) {
                    resultMap.put("code",502);
                    resultMap.put("msg","账号或密码错误");
                    return resultMap;
                }
                String new_password= MD5Util.MD5(password);
                if(merchant.getPassword().equals(new_password)){
                    resultMap.put("code",201);
                    resultMap.put("msg","欢迎管理员,登陆成功");
                    //传回用户信息，敏感信息(密码)不传
                    merchant.setPassword("");
                    resultMap.put("data", merchant);
                    return resultMap;
                }
                else{
                    resultMap.put("code",501);
                    resultMap.put("msg","账号或密码错误");
                    return resultMap;
                }
            }
            else if(flag=='p'){
                Courier courier = courierMapper.queryByAccount(account);
                System.out.println(courier);
                if (courier == null) {
                    resultMap.put("code",502);
                    resultMap.put("msg","账号或密码错误");
                    return resultMap;
                }
                String new_password= MD5Util.MD5(password);
                System.out.println(new_password);
                if(courier.getPassword().equals(new_password)){
                    resultMap.put("code",202);
                    resultMap.put("msg","登陆成功");
                    //传回用户信息，敏感信息(密码)不传
                    courier.setPassword("");
                    resultMap.put("data",courier);
                    return resultMap;
                }
                else{
                    resultMap.put("code",501);
                    resultMap.put("msg","账号或密码错误");
                    return resultMap;
                }
            }
            else {
                User user = userMapper.queryByAccount(account);
                if (user == null) {
                    resultMap.put("code", 502);
                    resultMap.put("msg", "账号或密码错误");
                    return resultMap;
                }
                String new_password = MD5Util.MD5(password);
                if (user.getPassword().equals(new_password)) {
                    resultMap.put("code", 200);
                    resultMap.put("msg", "登陆成功");
                    //传回用户信息，敏感信息(密码)不传
                    user.setPassword("");
                    resultMap.put("data", user);
                    return resultMap;
                } else {
                    resultMap.put("code", 501);
                    resultMap.put("msg", "账号或密码错误");
                    return resultMap;
                }
            }
        }catch (Exception e){
            resultMap.put("code",500);
            resultMap.put("msg","系统崩了，一会再来");
            return resultMap;
        }

    }

    @Override
    public Map register(String account, String user_name, String password) {
        Map resultMap = new HashMap<>();
        try {
            //判断账号是否重复
            User judgeResult = userMapper.queryByAccount(account);
            if (judgeResult != null){
                resultMap.put("code",501);
                resultMap.put("msg","账号重复，请修改账号名重试");
            }else{
                String new_password = MD5Util.MD5(password);
                User user = new User();
                user.setAccount(account);
                user.setPassword(new_password);
                user.setUserName(user_name);
                int result = userMapper.registerAccount(user);
                if(result != 0){
                    resultMap.put("code",200);
                    resultMap.put("msg","注册成功");
                }else {
                    resultMap.put("code",501);
                    resultMap.put("msg","注册失败，请稍后重试");
                }
            }
        }catch (Exception e){
            System.out.println(e);
            resultMap.put("code",500);
            resultMap.put("msg","系统崩了，一会再来");
        }
        return resultMap;
    }

    //修改密码
    @Override
    public Map modifyPasswordByAccount(String account,String oldPassword,String newPassword) {
        Map resultMap = new HashMap();
        //判断此账号是什么类型的账号
        char flag = account.charAt(0);
        //给传过来的密码加密
        String md5OldPassword = MD5Util.MD5(oldPassword);
        String md5NewPassword = MD5Util.MD5(newPassword);
        try {
            if (flag == 'd') {//店长
                //先判断旧密码是否正确
                Merchant judgeResult = merchantMapper.queryByAccount(account);
                if (judgeResult.getPassword().equals(md5OldPassword)){
                    //相同，进行修改密码
                    Merchant merchant = new Merchant();
                    merchant.setAccount(account);
                    merchant.setPassword(md5NewPassword);
                    Integer result = merchantMapper.modifyPasswordByAccount(merchant);
                    if (result != 0) {
                        resultMap.put("code",200);
                        resultMap.put("msg","密码修改成功");
                    }else {
                        resultMap.put("code",501);
                        resultMap.put("msg","密码修改失败，请重试");
                    }
                }else {
                    resultMap.put("code", 501);
                    resultMap.put("msg", "密码输入错误，请检查后重试");
                }
            }else if(flag == 'p'){//员工
                //先判断旧密码是否正确
                Courier judgeResult = courierMapper.queryByAccount(account);
                if (judgeResult.getPassword().equals(md5OldPassword)){
                    //相同，进行修改密码
                    Courier courier = new Courier();
                    courier.setAccount(account);
                    courier.setPassword(md5NewPassword);
                    Integer result = courierMapper.modifyPasswordByAccount(courier);
                    if (result != 0) {
                        resultMap.put("code", 200);
                        resultMap.put("msg", "密码修改成功");
                    } else {
                        resultMap.put("code", 501);
                        resultMap.put("msg", "密码修改失败，请重试");
                    }
                }else {
                    resultMap.put("code", 501);
                    resultMap.put("msg", "密码输入错误，请检查后重试");
                }
            }else {//普通用户
                //先判断旧密码是否正确
                User judgeResult = userMapper.queryByAccount(account);
                if (judgeResult.getPassword().equals(md5OldPassword)) {
                    //相同，进行修改密码
                    User user = new User();
                    user.setAccount(account);
                    user.setPassword(md5NewPassword);
                    Integer result = userMapper.modifyPasswordByAccount(user);
                    if (result != 0) {
                        resultMap.put("code", 200);
                        resultMap.put("msg", "密码修改成功");
                    } else {
                        resultMap.put("code", 501);
                        resultMap.put("msg", "密码修改失败，请重试");
                    }
                } else {
                    resultMap.put("code", 501);
                    resultMap.put("msg", "密码输入错误，请检查后重试");
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            resultMap.put("code",500);
            resultMap.put("msg","系统崩了，一会再来");
        }
        return resultMap;
    }

    @Override
    public User queryByAccount(String account) {
        User user= userMapper.queryByAccount(account);
        if (user!=null){
        user.setPassword("");}
        return user;
    }


    @Override
    public Map updateUser(String userName, String address, String telephone, String account) {
        Map resultMap = new HashMap<>();
        try {
            //更改用户信息
            User user = new User();
            user.setUserName(userName);
            user.setAddress(address);
            user.setTelephone(telephone);
            user.setAccount(account);
            int result = userMapper.updateUser(user);
            if (result != 0) {
                resultMap.put("code", 200);
                resultMap.put("msg", "更新成功");
            } else {
                resultMap.put("code", 501);
                resultMap.put("msg", "更新失败");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return resultMap;
    }
}
