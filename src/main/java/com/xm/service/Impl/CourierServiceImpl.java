package com.xm.service.Impl;

import com.xm.mapper.CourierMapper;
import com.xm.mapper.OrderMapper;
import com.xm.pojo.Courier;
import com.xm.pojo.Order;
import com.xm.pojo.Salary;
import com.xm.pojo.User;
import com.xm.service.CourierService;
import com.xm.utils.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CourierServiceImpl implements CourierService {

    @Autowired
    CourierMapper courierMapper;
    @Autowired
    OrderMapper orderMapper;
    public List<Salary> queryCourierMoney(int courierId){
        return courierMapper.queryCourierMoney(courierId);
    }
    @Override
    public List<Courier> queryAllCourier() {
        return courierMapper.queryAllCourier();
    }
    //删除员工并返回提示信息
    @Override
    public Map<Object, Object> deleteCourier(int courierId) {
        int a=courierMapper.deleteCourier(courierId);
        Map<Object,Object> map=new HashMap<Object, Object>();
        if (a==1){
            map.put("msg","删除成功");
            map.put("code",200);
            return map;
        }else {
            map.put("msg","删除失败");
            map.put("code",500);
            return map;
        }

    }
    //查找某一个员工
    public Courier findCourier(int courierId){
        List<Courier> list=queryAllCourier();
        Courier courier=new Courier();
        for (int i = 0; i<list.size(); i++){
            if(list.get(i).getCourierId()==courierId){
                courier=list.get(i);
                courier.setPassword("");
            }
        }
        return courier;
    }
    //修改员工
    public Map<Object,Object> updateCourier(int courierId,String courierName,String sex,String telephone){
        Map<Object,Object> map=new HashMap<Object, Object>();
        try {
            map.put("courierId",courierId);
            map.put("courierName",courierName);
            map.put("sex",sex);
            map.put("telephone",telephone);
            int a= courierMapper.updateCourier(map);
            map.clear();
               if (a==1){
                   map.put("code",200);
                   map.put("msg","修改成功");
                   return map;
               }else {
                   map.put("code",500);
                   map.put("msg","修改失败,可能没有这个人");
                   return map;
            }
        }catch (Exception e){
            map.put("code",500);
            map.put("msg","系统错误");
            return map;
        }
    }

    @Override
    public Map<Object, Object> addCourier(String courierName,String sex,String telephone,String account,String password) {
        Map<Object,Object> map=new HashMap<>();
            try {
                Courier courier=courierMapper.queryByAccount(account);
                if (courier==null) {
                    password= MD5Util.MD5(password);
                    Courier courier1=new Courier();
                    courier1.setCourierName(courierName);
                    courier1.setPassword(password);
                    courier1.setAccount(account);
                    courier1.setMoney(0);
                    courier1.setNumber(0);
                    courier1.setSex(sex);
                    courier1.setTelephone(telephone);

                int judgment = courierMapper.addCourier(courier1);
                if (judgment == 1) {
                    map.put("code", 200);
                    map.put("msg", "添加成功");
                    return map;
                }

                }
                else {
                    map.put("code", 502);
                    map.put("msg", "员工已经存在了");
                    return map;
                }

            } catch (Exception e) {
                map.put("code", 501);
                map.put("msg", "系统错误,添加失败");
                return map;
            }
        map.put("code", 501);
        map.put("msg", "系统错误,添加失败");
        return map;



    }


}
