package com.xm.service.Impl;

import com.xm.mapper.ClothesMapper;
import com.xm.pojo.Clothes;
import com.xm.service.ClothesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ClothesServiceImpl implements ClothesService {
    @Autowired
    ClothesMapper clothesMapper;

    //展示衣物表
    @Override
    public Map showClothesAll() {
        Map resultMap = new HashMap();
        try {
            List<Clothes> clothes = clothesMapper.showClothesAll();
            if (clothes.size() > 0) {
                resultMap.put("code",200);
                resultMap.put("data", clothes);
            }else {
                resultMap.put("code",501);
                resultMap.put("msg", "衣物表获取失败，请重试");
            }
        }catch (Exception e) {
            e.printStackTrace();
            resultMap.put("code",500);
            resultMap.put("msg", "系统崩了，一会再来");
        }
        return resultMap;
    }

    //添加衣物
    @Override
    public Map addClothes(String clothesName, double clothesMoney) {
        Map resultMap = new HashMap();
        try {
            //判断衣物名是否有重复
            Clothes judgeResult = clothesMapper.showClothesByClothesName(clothesName);
            if (judgeResult != null){
                resultMap.put("code", 501);
                resultMap.put("msg", "衣物名重复，请修改后重试");
            }else {
                Clothes clothes = new Clothes();
                clothes.setClothesName(clothesName);
                clothes.setClothesMoney(clothesMoney);
                int result = clothesMapper.addClothes(clothes);
                if (result != 0) {
                    resultMap.put("code",200);
                    resultMap.put("msg", "添加成功");
                }else {
                    resultMap.put("code", 501);
                    resultMap.put("msg", "添加失败，请稍后重试");
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            resultMap.put("code",500);
            resultMap.put("msg", "系统崩了，一会再来");
        }
        return resultMap;
    }

    //更新衣物
    @Override
    public Map updateClothesDataById(Integer clothesId, String clothesName, double clothesMoney) {
        Map resultMap = new HashMap();
        try {
            //判断衣物名是否有重复
            Clothes judgeResult = clothesMapper.showClothesByClothesName(clothesName);
            if (judgeResult != null && judgeResult.getClothesId() != clothesId){
                resultMap.put("code", 501);
                resultMap.put("msg", "衣物名重复，请修改后重试");
            }else {
                Clothes clothes = new Clothes();
                clothes.setClothesId(clothesId);
                clothes.setClothesName(clothesName);
                clothes.setClothesMoney(clothesMoney);
                int result = clothesMapper.updateClothesDataById(clothes);
                if (result != 0) {
                    resultMap.put("code", 200);
                    resultMap.put("msg", "更改成功");
                } else {
                    resultMap.put("code", 501);
                    resultMap.put("msg", "更改失败，请稍后重试");
                }
            }
        }catch (Exception e) {
            e.printStackTrace();
            resultMap.put("code",500);
            resultMap.put("msg", "系统崩了，一会再来");
        }
        return resultMap;
    }

    //删除衣物信息
    @Override
    public Map deleteClothesById(Integer clothesId) {
        Map resultMap = new HashMap();
        try {
            int result = clothesMapper.deleteClothesById(clothesId);
            if (result != 0) {
                resultMap.put("code",200);
                resultMap.put("msg", "删除成功");
            }else {
                resultMap.put("code", 501);
                resultMap.put("msg", "删除失败，请稍后重试");
            }
        }catch (Exception e) {
            e.printStackTrace();
            resultMap.put("code",500);
            resultMap.put("msg", "系统崩了，一会再来");
        }
        return resultMap;
    }

}
