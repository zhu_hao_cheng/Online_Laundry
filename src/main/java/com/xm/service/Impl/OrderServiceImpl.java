package com.xm.service.Impl;

import com.xm.mapper.*;
import com.xm.pojo.Courier;
import com.xm.pojo.Merchant;
import com.xm.pojo.Order;
import com.xm.pojo.User;
import com.xm.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.*;

import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    OrderMapper orderMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    CourierMapper courierMapper;
    @Autowired
    MerchantMapper merchantMapper;

    @Autowired
    NoticeMapper noticeMapper;

    @Override
    public List<Order> queryOrder(String account){
        return orderMapper.queryOrder(account);
    }


    @Override
    public Map addOrder(int merchantId, String account, double money) {
        Map resultMap=new HashMap();
        Order order = new Order();
        //日志Map
        Map<String,Object> noticeMap = new HashMap();
        User user =userMapper.queryByAccount(account);
        //读取本地时间
        Calendar calendar= Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        String local=sdf.format(calendar.getTime());
        try {
            if (userMapper.queryByAccount(account).getMoney()>=money){
                orderMapper.addOrder(merchantId,account,1,local,money);
                order.setUserName(user.getUserName());
                order.setAccount(account);
                order.setAddress(user.getAddress());
                order.setStateName("待接单");
                order.setBeginTime(local);
                order.setMoney(money);
                Merchant merchant= merchantMapper.queryByMerchantId(merchantId);
                int change = merchant.getNumbers()+1;
                merchantMapper.updateN(change,merchantId);
                //生成日志
                noticeMap.put("noticeTime",local);
                noticeMap.put("information","您有新的订单");
                noticeMap.put("account",merchantMapper.queryByMerchantId(merchantId).getAccount());
                noticeMap.put("flag",0);
                noticeMapper.addNotice(noticeMap);
                noticeMap.clear();
                noticeMap.put("account",account);
                noticeMap.put("money",userMapper.queryByAccount(account).getMoney()-money);
                userMapper.updateMoney(noticeMap);
                //返回码
                resultMap.put("code",200);
                resultMap.put("msg","订单生成成功");
                resultMap.put("data",order);
                return resultMap;
            }else{
                resultMap.put("code",500);
                resultMap.put("msg","订单生成失败，余额不足");
                return resultMap;
            }
        }catch (Exception e){
            System.out.println(e.toString());
            resultMap.put("code",501);
            resultMap.put("msg","服务器异常，请稍后再试");
            return resultMap;
        }


    }
    //配送员接单状态改变
    @Override
    public Map updateSC(int orderId,int courierId) {
        Map resultMap=new HashMap();
        //日志Map
        Map<String,Object> noticeMap = new HashMap();
        try {
            if (orderMapper.updateSC(2,courierId,orderId)){
                double money = orderMapper.getMoneyById(orderId);
                Courier courier = courierMapper.queryByCourierId(courierId);
                Merchant merchant = merchantMapper.queryByMerchantId(1);
                double x = 5;
                courierMapper.updateM(courier.getMoney()+(money/x),courierId);
                merchantMapper.updateM(merchant.getMoney()+money,1);
                resultMap.put("code",200);
                resultMap.put("msg","成功接取订单");
                //读取本地时间
                Calendar calendar= Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
                String local=sdf.format(calendar.getTime());
                //生成日志
                noticeMap.put("noticeTime",local);
                noticeMap.put("information","配送员"+courierMapper.queryByCourierId(courierId).getCourierName()+"已接收订单");
                noticeMap.put("account",orderMapper.queryOrdersByOrderId(orderId).getAccount());
                noticeMap.put("flag",0);
                noticeMapper.addNotice(noticeMap);
                return resultMap;
            }else {
                resultMap.put("code",500);
                resultMap.put("msg","接取订单失败，请刷新界面后重试");
                return resultMap;
            }
        }catch (Exception e){
            System.out.println(e.toString());
            resultMap.put("code",501);
            resultMap.put("msg","系统开小差了，请联系管理员");
            return resultMap;
        }
    }
    //洗衣店开始洗衣点击改变状态
    @Override
    public Map updateS1(int orderId) {
        Map resultMap=new HashMap();
        //日志Map
        Map<String,Object> noticeMap = new HashMap();
        try {
            if (orderMapper.updateS(3,orderId)){
                resultMap.put("code",200);
                resultMap.put("msg","衣服开始清洗，订单修改完成");
                //读取本地时间
                Calendar calendar= Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
                String local=sdf.format(calendar.getTime());
                //生成日志
                noticeMap.put("noticeTime",local);
                noticeMap.put("information","你的"+orderId+"号订单已开始清洗");
                noticeMap.put("account",orderMapper.queryOrdersByOrderId(orderId).getAccount());
                noticeMap.put("flag",0);
                noticeMapper.addNotice(noticeMap);
                return resultMap;
            }else {
                resultMap.put("code",500);
                resultMap.put("msg","订单修改失败");
                return resultMap;
            }
        }catch (Exception e){
            System.out.println(e.toString());
            resultMap.put("code",501);
            resultMap.put("msg","系统开小差了，请联系管理员");
            return resultMap;
        }
    }
    //洗衣完成后店家改变状态
    @Override
    public Map updateS2(int orderId) {
        Map resultMap=new HashMap();
        Map<String,Object> noticeMap = new HashMap();
        try {
            if (orderMapper.updateS(4,orderId)){
                //读取本地时间
                Calendar calendar= Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
                String local=sdf.format(calendar.getTime());
                //生成日志
                noticeMap.put("noticeTime",local);
                noticeMap.put("information","你的"+orderId+"号衣物已经洗完了，待送回");
                noticeMap.put("account",orderMapper.queryOrdersByOrderId(orderId).getAccount());
                noticeMap.put("flag",0);
                noticeMapper.addNotice(noticeMap);
                resultMap.put("code",200);
                resultMap.put("msg","衣服已洗完，订单修改完成");
                return resultMap;
            }else {
                resultMap.put("code",500);
                resultMap.put("msg","订单修改失败");
                return resultMap;
            }
        }catch (Exception e){
            System.out.println(e.toString());
            resultMap.put("code",501);
            resultMap.put("msg","系统开小差了，请联系管理员");
            return resultMap;
        }
    }
    //配送员送回点击改变状态
    @Override
    public Map updateS3(int orderId) {
        Map resultMap=new HashMap();
        Map<String,Object> noticeMap = new HashMap();
        Calendar calendar= Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        String local=sdf.format(calendar.getTime());
        try {
            if (orderMapper.updateS(5,orderId)){
                //生成日志
                noticeMap.put("noticeTime",local);
                noticeMap.put("information","你的"+orderId+"号衣物已送回");
                noticeMap.put("account",orderMapper.queryOrdersByOrderId(orderId).getAccount());
                noticeMap.put("flag",0);
                noticeMapper.addNotice(noticeMap);
                resultMap.put("code",200);
                resultMap.put("msg","衣服回送，订单修改完成");
                return resultMap;
            }else {
                resultMap.put("code",500);
                resultMap.put("msg","订单修改失败");
                return resultMap;
            }
        }catch (Exception e){
            System.out.println(e.toString());
            resultMap.put("code",501);
            resultMap.put("msg","系统开小差了，请联系管理员");
            return resultMap;
        }
    }
    //顾客收到衣服点击改变状态
    @Override
    public Map updateS4(int orderId) {
        Map resultMap=new HashMap();
        Map<String,Object> noticeMap = new HashMap();
        Calendar calendar= Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        String local=sdf.format(calendar.getTime());
        try {
            if (orderMapper.updateS(6,orderId)){
                orderMapper.updateET(local,orderId);
                //生成日志
                noticeMap.put("noticeTime",local);
                noticeMap.put("information","你的"+orderId+"订单衣物已经签收");
                noticeMap.put("account",orderMapper.queryOrdersByOrderId(orderId).getAccount());
                noticeMap.put("flag",0);
                noticeMapper.addNotice(noticeMap);
                //员工送订单数+1
                //查询员工id
                int courierId = orderMapper.queryOrdersByOrderId(orderId).getCourierId();
                //订单数+1
                int number = courierMapper.queryByCourierId(courierId).getNumber()+1;
                int result = orderMapper.updateOrdersNumber(number,courierId);
                resultMap.put("code",200);
                resultMap.put("msg","顾客确认收到衣服，订单修改完成");
                return resultMap;
            }else {
                resultMap.put("code",500);
                resultMap.put("msg","订单修改失败");
                return resultMap;
            }
        }catch (Exception e){
            System.out.println(e.toString());
            resultMap.put("code",501);
            resultMap.put("msg","系统开小差了，请联系管理员");
            return resultMap;
        }
    }

    //带送回订单
    @Override
    public Map updateS5(int orderId) {
        Map resultMap=new HashMap();
        Map<String,Object> noticeMap = new HashMap();
        Calendar calendar= Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
        String local=sdf.format(calendar.getTime());
        try {
            if (orderMapper.updateS(7,orderId)){
                //生成日志
                noticeMap.put("noticeTime",local);
                noticeMap.put("information",orderId+"号订单用户已点击可送回");
                noticeMap.put("account",courierMapper.queryByCourierId(orderMapper.queryOrdersByOrderId(orderId).getCourierId()).getAccount());
                noticeMap.put("flag",0);
                noticeMapper.addNotice(noticeMap);
                resultMap.put("code",200);
                resultMap.put("msg","用户已点击可送回，订单修改完成");
                return resultMap;
            }else {
                resultMap.put("code",500);
                resultMap.put("msg","订单修改失败");
                return resultMap;
            }
        }catch (Exception e){
            System.out.println(e.toString());
            resultMap.put("code",501);
            resultMap.put("msg","系统开小差了，请联系管理员");
            return resultMap;
        }
    }

    @Override
    public List<Order> queryAllOrder() {
        return orderMapper.queryAllOrder();
    }


    @Override
    public List<Order> grabOrdersList() {
        List<Order> ordersList=orderMapper.queryGrabOrdersList();

        return ordersList;
    }

    @Override
    public List<Order> queryOrdersByCourierId(int courierId){
        return orderMapper.queryOrdersByCourierId(courierId);
    }

}
