package com.xm.service.Impl;

import com.xm.mapper.CourierMapper;
import com.xm.mapper.MerchantMapper;
import com.xm.mapper.OrderMapper;
import com.xm.pojo.Courier;
import com.xm.pojo.Merchant;
import com.xm.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class MerchantServiceImpl implements MerchantService {
    @Autowired
    CourierMapper courierMapper;
    @Autowired
    MerchantMapper merchantMapper;

    @Autowired
    OrderMapper orderMapper;
    @Override
    public Map<Object,Object> totalChangeMoney(int courierId, int merchantId) {
        Map<Object,Object> resultMap=new HashMap<>();
        Map<Object,Object> merchantMap=new HashMap<>();
        Map<Object,Object> courierMap=new HashMap<>();
        Map<Object,Object> fountMap=new HashMap<>();
        try {
            Courier courier= courierMapper.queryByCourierId(courierId);
            Merchant merchant=merchantMapper.queryByMerchantId(merchantId);
            if(courier.getMoney()>=merchant.getMoney()){
                resultMap.put("code",503);
                resultMap.put("msg","钱钱不够老板!");
                return resultMap;
            }

            //扣店家的余额
            merchantMap.put("money",merchant.getMoney()- courier.getMoney());
            merchantMap.put("account",merchant.getAccount());
            Integer a = merchantMapper.changeMoney(merchantMap);
            //清空配送员当月数据
            Integer b=courierMapper.wipeData(courier.getAccount());
            //将配送员当月的数据保存
            courierMap.put("courierId",courier.getCourierId());
            courierMap.put("moneys",courier.getMoney());
            courierMap.put("numbers",courier.getNumber());

            Date date = new Date();
            SimpleDateFormat dateFormat= new SimpleDateFormat("yyyy-MM-dd :hh:mm:ss");
            courierMap.put("date",dateFormat.format(date));
            Integer c=courierMapper.saveData(courierMap);
            fountMap.put("merchantId",merchantId);
            fountMap.put("expend",courier.getMoney());
            fountMap.put("date",dateFormat.format(date));
            System.out.println(dateFormat.format(date));
            fountMap.put("courierId",courierId);
            Integer d=merchantMapper.addFount(fountMap);
            //over

            if(a==1&&b==1&&c==1&&d==1){
                resultMap.put("code",200);
                resultMap.put("msg","工资发放成功!");
                return resultMap;
            }

        }catch (Exception a){
            resultMap.put("code",500);
            resultMap.put("msg","服务器错误!");
            return resultMap;
        }
        resultMap.put("code",501);
        resultMap.put("msg","发生了未知错误！");
        return resultMap;
    }
    @Override
    public Integer feedBack(int orderId,int flag,int merchantId) {
        Map merchantMap = new HashMap();
        //通过商家id得到好评差评
        Merchant merchant=merchantMapper.queryByMerchantId(merchantId);
        int good = merchant.getGood();
        int differ = merchant.getDiffer();
        //好评差评数量
        if (flag == 0) {
            merchantMap.put("good", good++);
        }else if (flag == 1) {
            merchantMap.put("differ", differ++);
        }
        Map map = new HashMap<>();
        map.put("good",good);
        map.put("differ",differ);
        map.put("merchantId",merchantId);
        Integer back = merchantMapper.feedBack(map);
        //将订单状态改为已评价
        orderMapper.updateS(8,orderId);
        return back;
    }
    public Merchant querymerchantBymerchantId(int merchantId){
        Merchant merchant= merchantMapper.queryByMerchantId(merchantId);
        merchant.setPassword("");
        return merchant;
    }
}
