package com.xm.service.Impl;

import com.xm.mapper.BillMapper;
import com.xm.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Service
public class BillServiceImpl implements BillService {

    @Autowired
    BillMapper billMapper;

    @Override
    public Map insertBill(String account, double money) {
        Map map =new HashMap();
        Map resultMap = new HashMap();
        map.put("account",account);
        map.put("money",money);
        map.put("billId","");

        Calendar now = Calendar.getInstance();
        String year=now.get(Calendar.YEAR)+"";
        int mon=(now.get(Calendar.MONTH) + 1);
        int dat=now.get(Calendar.DAY_OF_MONTH);

        String yue=mon+""; //月份
        String ri=dat+"";  //日期
        if(mon<10){ //小于两位数前面补0
            yue="0"+mon;
        }
        if(dat<10) {//小于两位数前面补0
            ri = "0" + dat;
        }
        String billTime = year+"-"+yue+"-"+ri;
        map.put("billTime",billTime);

        try {
            int i = billMapper.insertBill(map);

            Object id=map.get("billId");
            if(i!=0){
                resultMap.put("code",200);
                resultMap.put("msg","确定充值？");
                resultMap.put("billId",id);
            }
            else {
                resultMap.put("code",501);
                resultMap.put("msg","充值失败");
            }
        }catch (Exception e){
            resultMap.put("code",500);
            resultMap.put("msg","充值失败");
        }
        return resultMap;
    }
}
