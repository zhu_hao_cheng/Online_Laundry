package com.xm.service;

import com.xm.pojo.Notice;

import java.util.List;
import java.util.Map;

public interface NoticeService {
    public List<Notice> queryNoticeByAccount(String account);

    public Map changeStage(Integer noticeId);
}
