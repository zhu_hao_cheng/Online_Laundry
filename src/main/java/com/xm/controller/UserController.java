package com.xm.controller;

import com.xm.pojo.Courier;
import com.xm.pojo.User;
import com.xm.service.UserService;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "/login",method = RequestMethod.POST)
    @ResponseBody
    public Map login(String account, String password){    //登录功能
        System.out.println(account);
        return userService.queryByAccount(account,password);
    }

    @RequestMapping(value = "/register",method = RequestMethod.POST)
    @ResponseBody
    public Map register(String account, String user_name,String password){   //登录功能
        return userService.register(account, user_name, password);
    }

    //修改密码
    @RequestMapping(value = "/modifyPassword",method = RequestMethod.POST)
    @ResponseBody
    public Map modifyPasswordByAccount(String account,String oldPassword,String newPassword){
        return userService.modifyPasswordByAccount(account, oldPassword, newPassword);
    }
    //用户基本信息的更改
    @RequestMapping(value = "/updateUser",method = RequestMethod.POST)
    @ResponseBody
    public Map updateUser(String userName, String address, String telephone, String account){
        return userService.updateUser(userName, address, telephone, account);
    }

    @RequestMapping(value = "/queryByAccount",method = RequestMethod.POST)
    @ResponseBody
    public void queryByAccount(String account, HttpServletResponse response){    //登录功能
        try {
            User user= userService.queryByAccount(account);
                JSONArray data = JSONArray.fromObject(user);
                response.setCharacterEncoding("utf-8");
                PrintWriter respWriter = response.getWriter();
                respWriter.append(data.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}