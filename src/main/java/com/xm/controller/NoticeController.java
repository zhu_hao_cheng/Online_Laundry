package com.xm.controller;

import com.xm.pojo.Courier;
import com.xm.pojo.Notice;
import com.xm.service.NoticeService;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class NoticeController {

    @Autowired
    NoticeService noticeService;

    @RequestMapping(value = "/Notice",method = RequestMethod.POST)
    @ResponseBody
    public void queryNoticeByAccount(String account,HttpServletResponse response){
        try{
            List<Notice> list = noticeService.queryNoticeByAccount(account);
            JSONArray data = JSONArray.fromObject(list);
            response.setCharacterEncoding("utf-8");
            PrintWriter respWriter = response.getWriter();
            respWriter.append(data.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/ChangeStage",method = RequestMethod.POST)
    @ResponseBody
    public Map changeStage(int noticeId){
        return noticeService.changeStage(noticeId);
    }


}
