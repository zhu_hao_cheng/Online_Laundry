package com.xm.controller;

import com.xm.service.FundsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class FundsController {
    @Autowired
    FundsService fundsService;

    //展示资金流水
    @RequestMapping(value = "/showFundsAll",method = RequestMethod.POST)
    @ResponseBody
    public Map showFundsAll(){
        return fundsService.showFundsAll();
    }

    //展示当月总收入
    @RequestMapping(value = "/showIncome",method = RequestMethod.POST)
    @ResponseBody
    public Map showIncome(String dateTime) {
        return fundsService.showIncome(dateTime);
    }

    //展示从开店到现在总收入
    @RequestMapping(value = "/showIncomeAll",method = RequestMethod.POST)
    @ResponseBody
    public Map showIncomeAll() {
        return fundsService.showIncomeAll();
    }
}
