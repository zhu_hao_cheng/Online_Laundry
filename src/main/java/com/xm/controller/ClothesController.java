package com.xm.controller;

import com.xm.service.ClothesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class ClothesController {
    @Autowired
    ClothesService clothesService;

    @RequestMapping(value = "/showClothesAll",method = RequestMethod.POST)
    @ResponseBody
    public Map showClothesAll(){
        //展示衣物表
        return clothesService.showClothesAll();
    }

    @RequestMapping(value = "/addClothes",method = RequestMethod.POST)
    @ResponseBody
    public Map addClothes(String clothesName,double clothesMoney){
        //添加衣物
        return clothesService.addClothes(clothesName, clothesMoney);
    }

    @RequestMapping(value = "/updateClothes",method = RequestMethod.POST)
    @ResponseBody
    public Map updateClothesDataById(Integer clothesId,String clothesName,double clothesMoney){
        //更新衣物信息
        return clothesService.updateClothesDataById(clothesId, clothesName, clothesMoney);
    }

    @RequestMapping(value = "/deleteClothes",method = RequestMethod.POST)
    @ResponseBody
    public Map deleteClothesById(Integer clothesId){
        //删除衣物信息
        System.out.println(clothesId+"======");
        return clothesService.deleteClothesById(clothesId);
    }
}
