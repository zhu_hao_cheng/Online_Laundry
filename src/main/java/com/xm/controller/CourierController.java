package com.xm.controller;

import com.xm.pojo.Courier;
import com.xm.pojo.User;
import com.xm.service.CourierService;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

@Controller
public class CourierController {
    @Autowired
    CourierService courierService;
    //找到配送员的流水
    @RequestMapping(value = "/queryCourierMoney",method = RequestMethod.POST)
    @ResponseBody
    public void queryCourierMoney(int courierId, HttpServletResponse response){
        try{
            List list = courierService.queryCourierMoney(courierId);
            System.out.println(list);
            JSONArray data = JSONArray.fromObject(list);
            response.setCharacterEncoding("utf-8");
            PrintWriter respWriter = response.getWriter();
            respWriter.append(data.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //查看所有员工
    @RequestMapping(value = "/courierList",method = RequestMethod.POST)
    @ResponseBody
    public void courierList(HttpServletResponse response){
        List<Courier> list= courierService.queryAllCourier();
        try {
            JSONArray data = JSONArray.fromObject(list);
            response.setCharacterEncoding("utf-8");
            PrintWriter respWriter = response.getWriter();
            respWriter.append(data.toString());

        }catch (Exception a){
            a.printStackTrace();
        }
    }

    //删除某一个员工
    @RequestMapping(value = "/deleteCourier",method = RequestMethod.POST)
    @ResponseBody
    public Map<Object,Object> deleteCourier(int courierId){
        return courierService.deleteCourier(courierId);
    }
    //查找某一个员工
    @RequestMapping(value = "/findCourier",method = RequestMethod.POST)
    @ResponseBody
    public void findCourier(int courierId,HttpServletResponse response){
        try {
            Courier courier=courierService.findCourier(courierId);
            JSONArray data = JSONArray.fromObject(courier);
            response.setCharacterEncoding("utf-8");
            PrintWriter respWriter = response.getWriter();
            respWriter.append(data.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    //修改员工信息
    @RequestMapping(value = "/updateCourier",method = RequestMethod.POST)
    @ResponseBody
    public Map<Object,Object> updateCourier(int courierId,String courierName,String sex,String telephone){
        return courierService.updateCourier(courierId,courierName,sex,telephone);
    }
    //添加员工
    @RequestMapping(value = "/addCourier",method = RequestMethod.POST)
    @ResponseBody
    public Map<Object,Object> addCourier(String courierName,String sex,String telephone,String account,String password){
        return courierService.addCourier(courierName,sex,telephone,account,password);
    }


}
