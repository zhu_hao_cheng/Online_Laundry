package com.xm.controller;

import com.xm.service.BillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
public class BillController {

    @Autowired
    BillService billService;

    @RequestMapping(value = "/insertBill" , method = RequestMethod.POST ) //新增充值订单
    @ResponseBody
    public Map insertBill(String account,double money){
        return billService.insertBill(account,money);
    }
}
