package com.xm.controller;


import com.xm.pojo.Courier;
import com.xm.pojo.Merchant;
import com.xm.service.CourierService;
import com.xm.service.MerchantService;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

@Controller
public class MerchantController {

    @Autowired
    MerchantService merchantService;
    @Autowired
    CourierService courierService;


    @RequestMapping(value = "/money",method = RequestMethod.POST)
    @ResponseBody  //店家发放工资操作
    public Map<Object,Object> totalChangeMoney(int courierId,int merchantId){
        return merchantService.totalChangeMoney(courierId,merchantId);
    }
    //反馈
    @RequestMapping(value = "/feedBack",method = RequestMethod.POST)
    @ResponseBody
    public int feedBack(int orderId, int flag,int merchantId){
        return merchantService.feedBack(orderId,flag,merchantId);
    }

    @RequestMapping(value = "/querymerchantBymerchantId",method = RequestMethod.POST)
    @ResponseBody
    public void querymerchantBymerchantId(int merchantId , HttpServletResponse response){
        try{
            Merchant merchant=merchantService.querymerchantBymerchantId(merchantId);
            System.out.println(merchant);
            JSONArray data = JSONArray.fromObject(merchant);
            response.setCharacterEncoding("utf-8");
            PrintWriter respWriter = response.getWriter();
            respWriter.append(data.toString());
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
