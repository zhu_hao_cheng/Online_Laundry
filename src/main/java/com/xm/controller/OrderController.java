package com.xm.controller;

import com.xm.pojo.Order;
import com.xm.service.CourierService;
import com.xm.service.OrderService;
import net.sf.json.JSONArray;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.List;
import java.util.Map;

@Controller
public class OrderController {
    @Autowired
    OrderService orderService;

    @RequestMapping(value = "/queryOrder",method = RequestMethod.POST)
    @ResponseBody
    public void queryOrder(String account, HttpServletResponse response){
        try{
            List list = orderService.queryOrder(account);
            JSONArray data = JSONArray.fromObject(list);
            response.setCharacterEncoding("utf-8");
            PrintWriter respWriter = response.getWriter();
            respWriter.append(data.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    @RequestMapping(value = "/queryAllOrder",method = RequestMethod.POST)
    @ResponseBody
    public void queryAllOrder(HttpServletResponse response){
        try{
            List list = orderService.queryAllOrder();
            JSONArray data = JSONArray.fromObject(list);
            response.setCharacterEncoding("utf-8");
            PrintWriter respWriter = response.getWriter();
            respWriter.append(data.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/grabOrdersList",method = RequestMethod.POST)
    @ResponseBody
    public void grabOrdersList(HttpServletResponse response){  //抢单功能
        List<Order> list= orderService.grabOrdersList();
        try {
            JSONArray data = JSONArray.fromObject(list);
            response.setCharacterEncoding("utf-8");
            PrintWriter respWriter = response.getWriter();
            respWriter.append(data.toString());
        }catch (Exception a){
            a.printStackTrace();
        }
    }
    @RequestMapping(value = "/initializeOrder",method = RequestMethod.POST)
    @ResponseBody
    public Map initializeOrder(int merchantId, String account, double money){
        return orderService.addOrder(merchantId,account,money);
    }
    @ResponseBody
    @RequestMapping(value = "/acceptOrder",method = RequestMethod.POST)
    public Map acceptOrder(int orderId,int courierId) {
        return orderService.updateSC(orderId, courierId);
    }
    @ResponseBody
    @RequestMapping(value = "/washOrder",method = RequestMethod.POST)
    public Map WashOrder(int orderId) {
        return orderService.updateS1(orderId);
    }
    @ResponseBody
    @RequestMapping(value = "/washedOrder",method = RequestMethod.POST)
    public Map WashedOrder(int orderId) {
        return orderService.updateS2(orderId);
    }
    @ResponseBody
    @RequestMapping(value = "/backOrder",method = RequestMethod.POST)
    public Map backOrder(int orderId) {
        return orderService.updateS3(orderId);
    }
    @ResponseBody
    @RequestMapping(value = "/finishOrder",method = RequestMethod.POST)
    public Map finishOrder(int orderId) {
        return orderService.updateS4(orderId);
    }

    //带送回订单
    @ResponseBody
    @RequestMapping(value = "/waitBackOrder",method = RequestMethod.POST)
    public Map waitBackOrder(int orderId) {
        return orderService.updateS5(orderId);
    }

    //配送员根据用户名查询用户订单信息
    @RequestMapping(value = "/queryOrdersByCourierId",method = RequestMethod.POST)
    @ResponseBody
    public void queryOrdersByCourierId(int courierId, HttpServletResponse response){    //登录功能
        try {
            List<Order> list = orderService.queryOrdersByCourierId(courierId);
            JSONArray data = JSONArray.fromObject(list);
            response.setCharacterEncoding("utf-8");
            PrintWriter respWriter = response.getWriter();
            respWriter.append(data.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
